#!/bin/bash

debug=1

# check it the process is running
# check for the PID file existance
# if it doesn't exist then run our program
# if it does exist then check if the PID is real, and if not run it
# NOTE: for this program this needs to be run as root user

PY_DIR="/opt/fake-voicemail"
VM_DIR="/var/www/default/voicemail"
DATE=`date +"%Y-%m-%d %H:%M:%S"`

if [ ! -e "$PY_DIR/voicemail.pid" ]; then
    cd "$VM_DIR"
    rm -rf *.txt
    if [ $debug -eq 1 ]; then 
        echo "== $DATE ==" | tee "$VM_DIR/status.txt"
        echo "=========================" | tee -a "$VM_DIR/status.txt"
        echo "> Launching voicemail " | tee -a "$VM_DIR/status.txt"
    fi  
    cd "$PY_DIR"
    python3 "$PY_DIR/voicemail.py" --config="$PY_DIR/config.ini"
else
    PID=`cat "$PY_DIR/voicemail.pid"`
    if ! ps -p $PID > /dev/null; then
        cd "$VM_DIR"
        rm -rf *.txt
        if [ $debug -eq 1 ]; then 
            echo "== $DATE ==" | tee "$VM_DIR/status.txt"
            echo "=========================" | tee -a "$VM_DIR/status.txt"
            echo "> Launching voicemail " | tee -a "$VM_DIR/status.txt"
        fi
        cd "$PY_DIR"
        python3 "$PY_DIR/voicemail.py" --config="$PY_DIR/config.ini"
    else
        if [ $debug -eq 1 ]; then 
            echo "== $DATE ==" | tee "$VM_DIR/status.txt"
            echo "=========================" | tee -a "$VM_DIR/status.txt"
            echo "> Voicemail running: $PID" | tee -a "$VM_DIR/status.txt"
        fi
    fi
fi

exit