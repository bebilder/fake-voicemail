# system libraries
import os, sys, getopt, configparser, shlex
from datetime import datetime
import time
import subprocess

# raspberry pi specific
from gpiozero import Button, LED, RGBLED

# logging/info printing
def logger(log_file: str, debug: bool, message: str):
    ts = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    if debug == True: print(ts + ' |' + message)

    log = open(log_file, 'a')
    log.write('\n')
    log.write(ts + ' |' + message)
    log.close()

# main function
if __name__ == "__main__":
    # fetch info from the config file
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["config="])

    except (Exception, getopt.GetoptError):
        print("Specify a config file with --config=<file name>")
        sys.exit(1)

    if len(opts) < 1:
        print("Specify a file with --config=<file name>")
        sys.exit(1)

    config_file = None

    for o, a in opts:
        if o == "--config":
            config_file = a

    config = configparser.ConfigParser()
    config.read(config_file)

    # output pid file
    pid = os.getpid()
    pid_f = open(config['GENERAL']['PY_DIR'] + '/voicemail.pid', 'w')
    pid_f.write(str(pid))
    pid_f.close()

    # set up log file
    log_date = datetime.now().strftime("%Y-%m-%d_%H.%M.%S")
    log_file = config['GENERAL']['VM_DIR'] + '/log_' + log_date + '.txt'
    log = open(log_file, 'w')
    log.write('== Running Voicemail: ' + log_date + ' == \n')
    log.close()

    # set up debugging
    debug = config['GENERAL'].getboolean('DEBUG')
    key = 'x'
    newkey = 'x'

    # set up raspberry pi gpio
    reset_btn = Button(int(config['GPIO']['RESET_BTN']), pull_up=True)
    phone_btn = Button(int(config['GPIO']['PHONE_BTN']), pull_up=True)
    led = RGBLED(
        int(config['GPIO']['RGB_RED']), 
        int(config['GPIO']['RGB_GRN']),
        int(config['GPIO']['RGB_BLU']) )

    logger(log_file, debug, "~~> Starting Process Loop\n")
    while True:

        # set grn led to signal loop reset
        led.color = (0, 1, 0)

        if debug == True:
            key = input("r to start new recording, q to quit: ")
        
        # detect signal for recording process
        if key == 'q' or reset_btn.is_pressed:
            logger(log_file, debug, "~~> Exiting, bye!")
            break
        elif key == 'r' or phone_btn.is_pressed == False:
            logger(log_file, debug, "+-> Starting a New Recording!")

            # set ylw led to signal start of recording
            led.color = (1, 0.25, 0)

            # wait a couple seconds for listener
            time.sleep(3)

            # set recording file name
            now = datetime.now()
            filename = "voicemail_" + now.strftime("%Y-%m-%d_%H.%M.%S") + ".mp3"
            filepath = config['GENERAL']['VM_DIR'] + "/" + filename

            # play the greeting
            # this uses the default system audio device
            logger(log_file, debug, "|-> playing greeting")
            os.system('mpg123 -q ' + config['GENERAL']['GREETING'])

            # record the message asynchronously
            # ffmpeg -f alsa -ac 1 -i hw:1 -filter:a "volume=0.5" -acodec libmp3lame -b:a 320k /var/www/default/voicemail/test.mp3
            logger(log_file, debug, "|-> ffmpeg recording started")
            ffmpeg_cmd = 'ffmpeg -f alsa -ac 1 -i hw:' + config['AUDIO']['INPUT'] + ' -filter:a "volume=' + config['AUDIO']['VOLUME'] + '" -acodec libmp3lame -b:a ' + config['AUDIO']['BITRATE'] + ' ' + filepath
            recording = subprocess.Popen(shlex.split(ffmpeg_cmd), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True)

            # wait a couple seconds for ffmpeg to initialize
            #time.sleep(2)

            # set red led to signal recording in progress
            led.color = (1, 0, 0)

            # detect button press
            # when pressed kill the recording process and restart loop
            while True:
                if debug == True:
                    newkey = input("s to stop: ")
                
                if newkey == 's' or phone_btn.is_pressed or reset_btn.is_pressed:
                    logger(log_file, debug, "|-> ffmpeg recording stop")
                    
                    # stop recording
                    recording.communicate(input='q')[0]
                    recording.wait()

                    # sleep for a little bit in case of button jitter
                    time.sleep(3)
                    
                    break
                else:
                    continue

            logger(log_file, debug, "|-> loop reset\n")
            continue
        else:
            continue

    sys.exit(0)
