# system libraries
import os, sys, getopt, configparser
import time

# raspberry pi specific
from gpiozero import Button, LED, RGBLED    

# main function
if __name__ == "__main__":
    # fetch info from the config file
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["config="])

    except (Exception, getopt.GetoptError):
        print("Specify a config file with --config=<file name>")
        sys.exit(1)

    if len(opts) < 1:
        print("Specify a file with --config=<file name>")
        sys.exit(1)

    config_file = None

    for o, a in opts:
        if o == "--config":
            config_file = a

    config = configparser.ConfigParser()
    config.read(config_file)

    # set up raspberry pi gpio
    reset_btn = Button(int(config['GPIO']['RESET_BTN']), pull_up=True)
    phone_btn = Button(int(config['GPIO']['PHONE_BTN']), pull_up=True)
    led = RGBLED(
        int(config['GPIO']['RGB_RED']), 
        int(config['GPIO']['RGB_GRN']),
        int(config['GPIO']['RGB_BLU']) )

    print("==== TESTING ====")

    # red led
    print("==> RED")
    led.color = (1, 0, 0)
    time.sleep(3)

    # green led
    print("==> GREEN")
    led.color = (0, 1, 0)
    time.sleep(3)

    # blue led
    print("==> BLUE")
    led.color = (0, 0, 1)
    time.sleep(3)

    # test btn1
    print("==> RESET BTN")
    reset_btn.wait_for_press()

    # test btn2
    print("==> PHONE BTN")
    phone_btn.wait_for_press()
