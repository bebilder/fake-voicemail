#!/bin/bash

VM_DIR="/var/www/default/voicemail"

cd "$VM_DIR"

# remove old voicemail archive
if [ -e "$VM_DIR/voicemails.zip" ]; then
    rm -rf "$VM_DIR/voicemails.zip"
fi

# create new archive
zip voicemails.zip *.mp3

exit 0